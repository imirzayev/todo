from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


# Custom User Manager
class UserAccountManager(BaseUserManager):
    def create_user(self, user_name, password=None):
        if not user_name:
            raise ValueError('Users must have username!')

        user = self.model(
            user_name=user_name
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, user_name, password=None):
        user = self.create_user(user_name, password)

        user.is_admin = True

        user.save(using=self._db)

        return user


# Custom User Model
class UserAccount(AbstractBaseUser):
    user_name = models.CharField(max_length=100, unique=True, verbose_name='username')
    email = models.EmailField(max_length=255, verbose_name='email address')

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'user_name'
    EMAIL_FIELD = 'email'

    objects = UserAccountManager()

    def __str__(self):
        return self.user_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

