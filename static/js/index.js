let wrapper = document.getElementById('wrapper')
let taskCreate = document.getElementById('form')
let url_task_list = '/api/tasks/'
let url_task_create = '/api/task_create/'
let url_task_delete = '/api/task_delete/'
let url_task_update = '/api/task_update/'
let edit = false


getTaskList()
    .then(data => {
        console.log(data)
        populateTasks(data)
    })


taskCreate.addEventListener('submit', function (e) {
    if (!document.getElementById('task-textarea').value) {
        e.preventDefault()
        return
    }

    e.preventDefault()
    const taskName = document.getElementById('task-textarea').value
    if (!edit) {
        createTask({name: taskName})
            .then(data => {
                console.log(data)
                document.getElementById('form').reset()
                getTaskList()
                    .then(data => {
                        populateTasks(data)
                    })
            })
    }

})

async function getTaskList() {
    try {
        const response = await fetch(url_task_list, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(data),
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}


async function createTask(data = {}) {
    try {
        const response = await fetch(url_task_create, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}

async function deleteTask( data = {}) {
    var delete_task = url_task_delete + data.id
    try {
        const response = await fetch(delete_task, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Content-Type': 'application/json'
            },
        })
        return response
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}


async function updateTask(id, data = {}) {
    edit = false
    const update_task = url_task_update + id + '/'

    try {
        const response = await fetch(update_task, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}


function populateTasks(data) {
    wrapper.innerHTML = ''
    for (let i in data) {
        let counter = parseInt(i) + 1
        let task = `<div class="task">
                <div class="task-info">
                    <a class="task-link" data-id="${data[i].id}" href=""><h4 class="task-name ${data[i].completed ? 'completed' : ''}">${counter}. ${data[i].name}</h4></a>
                </div>
                <div class="buttons">
                    <button class="btn btn-info edit" data-id="${data[i].id}">edit</button>
                    <button class="btn btn-danger delete" data-id="${data[i].id}">delete</button>
                </div>
            </div>`
        wrapper.innerHTML += task
    }


    let deletes = document.getElementsByClassName('delete')
    for(let i = 0; i < deletes.length; i++) {
        deletes[i].addEventListener('click', function (e) {
            const task_id = this.dataset.id

            deleteTask({id: task_id})
                .then(response =>{
                    if (response.ok)
                        console.log('deleted')
                    getTaskList()
                        .then(data => {
                            populateTasks(data)
                        })
                })
        })
    }

    let edits = document.getElementsByClassName('edit')
    let taskNames = document.getElementsByClassName('task-name')
    for(let i = 0; i < edits.length; i++) {
        edits[i].addEventListener('click', function (e) {
            document.getElementById('button').innerHTML = 'Update'
            const task_id = this.dataset.id
            edit = true

            let taskUpdate = taskNames[i].innerHTML.substring(3)
            console.log(taskUpdate)

            let taskUpdated = document.getElementById('task-textarea')
            taskUpdated.value = taskUpdate

            document.getElementById('form').addEventListener('submit', function (e) {
                e.preventDefault()
                if (edit){
                    updateTask(task_id, {name: taskUpdated.value, completed: false})
                        .then(response => {
                            document.getElementById('form').reset()
                            document.getElementById('button').innerHTML = `<i class="fa fa-paper-plane-o m-r-6" aria-hidden="true"></i>Create`
                            console.log(response)
                            getTaskList()
                                .then(data => {
                                    populateTasks(data)
                                })
                        })
                }
            })
        })
    }


    let tasks = document.getElementsByClassName('task-link')
    for (let i = 0; i < tasks.length; i++) {
        tasks[i].addEventListener('click', function (e) {
            e.preventDefault()
            const task_id = this.dataset.id
            let itemCompleted = !data[i].completed
            let taskName = taskNames[i].innerHTML.substring(3)
            console.log(taskName)
            updateTask(task_id, {name: taskName, completed: itemCompleted})
                .then(response => {
                    document.getElementById('form').reset()
                    document.getElementById('button').innerHTML = `<i class="fa fa-paper-plane-o m-r-6" aria-hidden="true"></i>Create`
                    console.log(response)
                    getTaskList()
                        .then(data => {
                            populateTasks(data)
                        })
                })
        })
    }
}