from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('tasks/', views.task_list, name='task_list'),
    path('task_create/', views.task_create, name='task_create'),
    path('task_details/<int:pk>/', views.task_details, name='task_details'),
    path('task_update/<int:pk>/', views.task_update, name='task_update'),
    path('task_delete/<int:pk>/', views.task_delete, name='task_delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
